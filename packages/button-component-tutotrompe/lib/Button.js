import React from "react"

export function Button({label = "Submit", handleClick = () => console.log('Click on button')}) {
    return <button onClick={handleClick}>{label}</button>
}
