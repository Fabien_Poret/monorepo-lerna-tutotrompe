import React, { useState } from "react"

import { Input } from "input-component-tutotrompe/build";
import { Button } from "button-component-tutotrompe/build";

export function Form({label = "Voici le nombre de caractères"}) {

    const [value, setValue] = useState('')
    const [count, setCount] = useState(0)

    return (
        <>
            <h1>{label} : {count}</h1>
            <Input handleChange={event => setValue(event.target.value)} />
            <Button handleClick={() => setCount(value.length)} />
        </>
    );
}

