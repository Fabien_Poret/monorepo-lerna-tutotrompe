"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Label = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Form = require("./Form");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  title: 'Example/Form',
  component: _Form.Form
};
exports.default = _default;

var Template = function Template(args) {
  return /*#__PURE__*/_react.default.createElement(_Form.Form, args);
};

var Label = Template.bind({});
exports.Label = Label;
Label.args = {
  label: "Voici le nombre d'éléphants"
};