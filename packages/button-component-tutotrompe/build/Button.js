"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Button = Button;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Button(_ref) {
  var _ref$label = _ref.label,
      label = _ref$label === void 0 ? "Submit" : _ref$label,
      _ref$handleClick = _ref.handleClick,
      handleClick = _ref$handleClick === void 0 ? function () {
    return console.log('Click on button');
  } : _ref$handleClick;
  return /*#__PURE__*/_react.default.createElement("button", {
    onClick: handleClick
  }, label);
}