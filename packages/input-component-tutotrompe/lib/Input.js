import React from "react"

export function Input({placeholder = "...", handleChange = event => console.log(event.target.value)}) {
    return <input type="text" placeholder={placeholder} onChange={handleChange}/>
}
