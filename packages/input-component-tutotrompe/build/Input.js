"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Input = Input;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Input(_ref) {
  var _ref$placeholder = _ref.placeholder,
      placeholder = _ref$placeholder === void 0 ? "..." : _ref$placeholder,
      _ref$handleChange = _ref.handleChange,
      handleChange = _ref$handleChange === void 0 ? function (event) {
    return console.log(event.target.value);
  } : _ref$handleChange;
  return /*#__PURE__*/_react.default.createElement("input", {
    type: "text",
    placeholder: placeholder,
    onChange: handleChange
  });
}